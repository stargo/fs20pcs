/* FS20PCS libusb-driver
 *
 * Copyright (c) 2013 Michael Gernoth <michael@gernoth.net>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#include <string.h>
#include <stdio.h>
#include <stdint.h>
#include <unistd.h>
#include <stdlib.h>
#include <math.h>
#include <libusb-1.0/libusb.h>

#define USB_TIMEOUT		10000

#define ID_VENDOR	0x18ef
#define ID_PRODUCT	0xe015

#define EXT_TO_SEC(ext) ((1 << ((ext & 0xf0) >> 4)) * (ext & 0x0f) * 0.25)

extern char *optarg;

/* Not in all libusb-1.0 versions, so we have to roll our own :-( */
static char * usb_strerror(int e)
{
	static char unknerr[256];

	switch (e) {
		case LIBUSB_SUCCESS:
			return "Success";
		case LIBUSB_ERROR_IO:
			return "Input/output error";
		case LIBUSB_ERROR_INVALID_PARAM:
			return "Invalid parameter";
		case LIBUSB_ERROR_ACCESS:
			return "Access denied (insufficient permissions)";
		case LIBUSB_ERROR_NO_DEVICE:
			return "No such device (it may have been disconnected)";
		case LIBUSB_ERROR_NOT_FOUND:
			return "Entity not found";
		case LIBUSB_ERROR_BUSY:
			return "Resource busy";
		case LIBUSB_ERROR_TIMEOUT:
			return "Operation timed out";
		case LIBUSB_ERROR_OVERFLOW:
			return "Overflow";
		case LIBUSB_ERROR_PIPE:
			return "Pipe error";
		case LIBUSB_ERROR_INTERRUPTED:
			return "System call interrupted (perhaps due to signal)";
		case LIBUSB_ERROR_NO_MEM:
			return "Insufficient memory";
		case LIBUSB_ERROR_NOT_SUPPORTED:
			return "Operation not supported or unimplemented on this platform";
		case LIBUSB_ERROR_OTHER:
			return "Other error";
	};
	snprintf(unknerr, sizeof(unknerr), "Unknown error code %d / 0x%02x", e, e);
	return unknerr;
}

libusb_device_handle *fs20pcs_find() {
	libusb_device_handle *devh = NULL;
	libusb_device **list;
	ssize_t cnt;
	ssize_t i;
	int err;

	cnt = libusb_get_device_list(NULL, &list);
	if (cnt < 0) {
		fprintf(stderr, "Can't get USB device list: %d\n", (int)cnt);
		return NULL;
	}

	for (i = 0; i < cnt; i++){
		struct libusb_device_descriptor desc;

		err = libusb_get_device_descriptor(list[i], &desc);
		if (err)
			continue;

		if ((desc.idVendor == ID_VENDOR) && (desc.idProduct == ID_PRODUCT)) {
			libusb_device *dev = list[i];

			err = libusb_open(dev, &devh);
			if (err) {
				fprintf(stderr, "Can't open device: %s\n", usb_strerror(err));
				return NULL;
			}

			err = libusb_detach_kernel_driver(devh, 0);
			if ((err != 0) && (err != LIBUSB_ERROR_NOT_FOUND)) {
				fprintf(stderr, "Can't detach kernel driver: %s\n", usb_strerror(err));
				return NULL;
			}

			err = libusb_claim_interface(devh, 0);
			if ((err != 0)) {
				fprintf(stderr, "Can't claim interface: %s\n", usb_strerror(err));
				return NULL;
			}

			return devh;
		}

	}

	return NULL;
}

int fs20pcs_send(libusb_device_handle *devh, unsigned char* send_data)
{
	unsigned char recv_data[5] = {0x00, 0x00, 0x00, 0x00, 0x00};
	int err;
	int cnt;
	int i;
	int ret;

	err = libusb_interrupt_transfer(devh, 0x01, send_data, 11, &cnt, USB_TIMEOUT);
	if (err) {
		fprintf(stderr, "Can't send data: %s\n", usb_strerror(err));
		return 0;
	}

	err = libusb_interrupt_transfer(devh, 0x81, recv_data, sizeof(recv_data), &cnt, USB_TIMEOUT);
	if (err) {
		fprintf(stderr, "Can't receive data: %s\n", usb_strerror(err));
		return 0;
	}

	if ((recv_data[0] != 0x02) ||
	    (recv_data[1] != 0x03) ||
	    (recv_data[2] != 0xa0)) {
	    fprintf(stderr, "Unexpected response: ");
	    for (i = 0; i < cnt; i++) {
		    fprintf(stderr, "0x%02x ", recv_data[i]);
	    }
	    fprintf(stderr, "\n");

	    return 0;
	}

	ret = 1;

	switch(recv_data[3]) {
		case 0x00:
			printf("Success");
			break;
		case 0x01:
			printf("Firmware: V%d.%d", ((recv_data[4] & 0xf0) >> 4) & 0x0f, recv_data[4] & 0x0f);
			break;
		case 0x02:
			printf("Unknown command");
			ret = 0;
			break;
		case 0x03:
			printf("Wrong length");
			ret = 0;
			break;
		case 0x04:
			printf("Aborted sending long press");
			break;
		case 0x05:
			printf("Nothing to stop");
			break;
		default:
			printf("Unknown response: 0x%02x 0x%02x", recv_data[3], recv_data[4]);
			ret = 0;
			break;
	}

	printf("\n");

	return ret;
}

unsigned char *parse_housecode(char *hc)
{
	static unsigned char housecode[2];
	char hc_fixed[9];
	char *ep;
	unsigned long val;
	int i;

	if (hc == NULL)
		return NULL;

	memset(housecode, 0, sizeof(housecode));

	switch(strlen(hc)) {
		case 6:
			if (strncmp(hc, "0x", 2)) {
				fprintf(stderr, "Not a 2 byte hexstring: %s\n", hc);
				return NULL;
			}
		case 4:
			val = strtoul(hc, &ep, 16);
			if (*ep != '\0') {
				fprintf(stderr, "Not a 2 byte hexstring: %s\n", hc);
				return NULL;
			}
			break;
		case 8:
			memset(hc_fixed, 0, sizeof(hc_fixed));
			for (i = 0; i < 8; i++) {
				if ((hc[i] < '1') || (hc[i] > '4')) {
					fprintf(stderr, "Not a valid ELV housecode: %s\n", hc);
					return NULL;
				}
				hc_fixed[i] = hc[i] - 1;
				val = strtoul(hc_fixed, &ep, 4);

				if (*ep != '\0') {
					fprintf(stderr, "Can't parse fixed ELV housecode: %s\n", hc_fixed);
					return NULL;
				}
			}
			break;
		default:
			fprintf(stderr, "Housecode has to be in hex (1234, 0x1234) or ELV (12341234) format!\n");
			return NULL;
			break;
	}

	housecode[0] = (val & 0xff00) >> 8;
	housecode[1] = (val & 0xff);

	return housecode;
}

int parse_addr(char *ad)
{
	int addr = -1;
	char ad_fixed[5];
	char *ep;
	unsigned long val;
	int i;

	if (ad == NULL)
		return -1;

	switch(strlen(ad)) {
		case 4:
			if (strncmp(ad, "0x", 2)) {
				memset(ad_fixed, 0, sizeof(ad_fixed));
				for (i = 0; i < 4; i++) {
					if ((ad[i] < '1') || (ad[i] > '4')) {
						fprintf(stderr, "Not a valid ELV address: %s\n", ad);
						return -1;
					}
					ad_fixed[i] = ad[i] - 1;
					val = strtoul(ad_fixed, &ep, 4);

					if (*ep != '\0') {
						fprintf(stderr, "Can't parse fixed ELV housecode: %s\n", ad_fixed);
						return -1;
					}
				}

				break;
			}
		case 2:
			val = strtoul(ad, &ep, 16);
			if (*ep != '\0') {
				fprintf(stderr, "Not a 1 byte hexstring: %s\n", ad);
				return -1;
			}
			break;
		default:
			fprintf(stderr, "Address has to be in hex (01, 0x01) or ELV (1112) format!\n");
			return -1;
			break;
	}

	addr = val & 0xff;

	return addr;
}

int parse_command(char *cmd)
{
	int command = -1;
	char *ep;
	unsigned long val;

	if (cmd == NULL)
		return -1;

	if (!strcasecmp(cmd, "off")) {
		command = 0x00;
	} else if (!strcasecmp(cmd, "on")) {
		command = 0x11;
	} else if (!strcasecmp(cmd, "toggle")) {
		command = 0x12;
	} else if (!strcasecmp(cmd, "dimup")) {
		command = 0x13;
	} else if (!strcasecmp(cmd, "dimdown")) {
		command = 0x14;
	} else if (!strcasecmp(cmd, "on-for-timer")) {
		command = 0x39;
	} else {
		val = strtoul(cmd, &ep, 16);
		if (*ep != '\0') {
			fprintf(stderr, "Not a 1 byte hexstring or alias: %s\n", cmd);
			return -1;
		}

		command = val & 0xff;
	}

	return command;
}

int parse_extension(char *ext)
{
	int extension = -1;
	char *ep;

	if (ext == NULL)
		return -1;

	if ((ext[strlen(ext)-1] == 's') ||
	    (ext[strlen(ext)-1] == 'S')) {
	    	double t;
		double diff = 1;
		uint8_t high = 0;
		uint8_t low = 0;
	    	uint8_t i;

		t = strtod(ext, &ep);
		if ((*ep != 's') && (*ep != 'S')) {
			fprintf(stderr, "Not a valid time in seconds: %s\n", ext);
			return -1;
		}

		/* time = 2^(high nibble) * (low nibble) * 0.25 , high may only be 12 */
		t /= 0.25;
#ifdef DEBUG
		printf("t: %f\n", t);
#endif

		for(i = 1; i <= 0xf; i++) {
			double h;
			double l;
			uint8_t i_l;
			double d;

#ifdef DEBUG
			printf("i: 0x%01x\n", i);
#endif

			h = t / i;
#ifdef DEBUG
			printf("h: %f\n", h);
#endif

			l = log2(h);
#ifdef DEBUG
			printf("l: %f\n", l);
#endif

			i_l = l;
			if (i_l > 12)
				i_l = 12;

			d = fabs(l - i_l);

#ifdef DEBUG
			printf("d: %f\n", d);
#endif

			if (d < diff) {
#ifdef DEBUG
				printf("Current best match!\n");
#endif
				diff = d;
				high = i_l;
				low = i;
			}
		}

#ifdef DEBUG
		printf("Got: %f, high: %01x, low: %01x\n", t, high, low);
#endif

		if ((high == 0) && (low == 0)) {
			fprintf(stderr, "Can't find extension byte for %s!\n", ext);
			return -1;
		}

		extension = ((high & 0xf) << 4) | (low & 0xf);
	} else {
		unsigned long val;

		val = strtoul(ext, &ep, 16);
		if (*ep != '\0') {
			fprintf(stderr, "Not a 1 byte hexstring or time: %s\n", ext);
			return -1;
		}

		extension = val & 0xff;
	}

	return extension;
}

void syntax(char *prog)
{
	fprintf(stderr, "Syntax: %s options\n\n", prog);
	fprintf(stderr, "Possible options:\n");
	fprintf(stderr, "\t-V\t\trequest firmware-version\n");
	fprintf(stderr, "\t-x\t\tabort sending long press\n");
	fprintf(stderr, "\t-h [ELV|hex]\thousecode in ELV- or hex-notation\n");
	fprintf(stderr, "The following options need an housecode:\n");
	fprintf(stderr, "\t-a [ELV|hex]\tdestination address\n");
	fprintf(stderr, "\t-s [hex|alias]\tsend command byte\n");
	fprintf(stderr, "\t-e [hex|time]\textension byte for command or time in seconds (e.g. 10s)\n");
	fprintf(stderr, "\t-r n\t\trepeat sending n times\n");
	fprintf(stderr, "\nCommand bytes (without extension byte):\n");
	fprintf(stderr, "hex\t\talias\tdescription\n");
	fprintf(stderr, "0x00\t\toff\tswitch off\n");
	fprintf(stderr, "0x01-0x10\t\tswitch on dimmed (0x01: 6.25%%, 0x02: 12.5%%, ..., 0x10: 100%%)\n");
	fprintf(stderr, "0x11\t\ton\tswitch on\n");
	fprintf(stderr, "0x12\t\ttoggle\ttoggle\n");
	fprintf(stderr, "0x13\t\tdimup\tdimup\n");
	fprintf(stderr, "0x14\t\tdimdown\tdimdown\n");
	fprintf(stderr, "0x15\t\t\tdimup, pause, dimdown, pause, ...\n");
	fprintf(stderr, "0x16\t\t\tstart/stop programming internal timer\n");
	fprintf(stderr, "0x17\t\t\tlearn housecode/address\n");
	fprintf(stderr, "0x18\t\t\toff for internal timer, then back on with current level\n");
	fprintf(stderr, "0x19\t\t\ton (100%%) for internal timer, then off\n");
	fprintf(stderr, "0x1a\t\t\ton (old level) for internal timer, then off\n");
	fprintf(stderr, "0x1b\t\t\tfactory reset\n");
	fprintf(stderr, "0x1e\t\t\ton (100%%) for internal timer, then old level\n");
	fprintf(stderr, "0x1f\t\t\ton (old level) for internal timer, then old state\n");
	fprintf(stderr, "\nCommand bytes (with timer as extension byte):\n");
	fprintf(stderr, "hex\t\talias\tdescription\n");
	fprintf(stderr, "0x20\t\t\tdim down to off while timer is running\n");
	fprintf(stderr, "0x21-0x30\t\tdim to (0x01: 6.25%%, 0x02: 12.5%%, ..., 0x10: 100%%) while timer is running\n");
	fprintf(stderr, "0x31\t\t\tdim to last level while timer is running\n");
	fprintf(stderr, "0x32\t\t\tdim to off, then after timer dim to off\n");
	fprintf(stderr, "0x33\t\t\tdimup now and switch off after timer\n");
	fprintf(stderr, "0x34\t\t\tdimdown now and switch off after timer\n");
	fprintf(stderr, "0x35\t\t\tdimup or dimdown (toggle) and switch off after timer\n");
	fprintf(stderr, "0x36\t\t\tprogram internal timer\n");
	fprintf(stderr, "0x38\t\t\toff for timer, then back on with current level\n");
	fprintf(stderr, "0x39\ton-for-timer\ton (100%%) for timer, then off\n");
	fprintf(stderr, "0x3a\t\t\ton (old level) for timer, then off\n");
	fprintf(stderr, "0x3c\t\t\tprogram internal ramp-up-time\n");
	fprintf(stderr, "0x3d\t\t\tprogram internal ramp-down-time\n");
	fprintf(stderr, "0x3e\t\t\ton (100%%) for timer, then old level\n");
	fprintf(stderr, "0x3f\t\t\ton (old level) for timer, then old state\n");
};

enum {
	NO_ACTION,
	REQUEST_FIRMWARE,
	SEND_COMMAND,
	ABORT_LONG_PRESS
};

#define DUPLICATE_ACTION	if (action != NO_ACTION) { \
					fprintf(stderr, "duplicate action specified!\n"); \
					exit(EXIT_FAILURE); }

int main(int argc, char **argv)
{
	unsigned char send_data[11];
	unsigned char *housecode = NULL;
	libusb_device_handle *devh = NULL;
	char *ep;
	int err;
	int opt;
	int action = NO_ACTION;
	int command = -1;
	int extension = 0;
	uint8_t repeat = 0;
	int addr = -1;

	memset(send_data, 0, sizeof(send_data));

	send_data[0] = 0x01;

	while ((opt = getopt(argc, argv, "Vxs:h:a:r:e:")) != -1) {
		switch(opt) {
			case 'V':
				DUPLICATE_ACTION;
				action = REQUEST_FIRMWARE;
				break;
			case 'x':
				DUPLICATE_ACTION;
				action = ABORT_LONG_PRESS;
				break;
			case 's':
				DUPLICATE_ACTION;
				action = SEND_COMMAND;

				command = parse_command(optarg);
				if (command == -1) {
					fprintf(stderr, "Can't parse command!\n");
					exit(EXIT_FAILURE);
				}
#ifdef DEBUG
				printf("Got command: %d\n", command);
#endif
				break;
			case 'h':
				housecode = parse_housecode(optarg);
				if (!housecode) {
					fprintf(stderr, "Can't parse housecode!\n");
					exit(EXIT_FAILURE);
				}
#ifdef DEBUG
				printf("Got housecode: 0x%02x%02x\n", housecode[0], housecode[1]);
#endif
				break;
			case 'a':
				addr = parse_addr(optarg);
				if (addr == -1) {
					fprintf(stderr, "Can't parse address!\n");
					exit(EXIT_FAILURE);
				}
#ifdef DEBUG
				printf("Got address: 0x%02x\n", addr);
#endif
				break;
			case 'r':
				repeat = strtoul(optarg, &ep, 10);
				if (*ep != '\0') {
					fprintf(stderr, "Can't parse repeat!\n");
					exit(EXIT_FAILURE);
				}
#ifdef DEBUG
				printf("Got repeat: %d\n", repeat);
#endif
				break;
			case 'e':
				extension = parse_extension(optarg);
				if (extension == -1) {
					fprintf(stderr, "Can't parse extension!\n");
					exit(EXIT_FAILURE);
				}
#ifdef DEBUG
				printf("Got extension: %d\n", extension);
#endif
				break;
			case ':':
			case '?':
			default:
				syntax(argv[0]);
				exit(EXIT_FAILURE);
				break;
		}
	}

	switch(action) {
		case REQUEST_FIRMWARE:
			send_data[1] = 0x01;
			send_data[2] = 0xf0;
			printf("Requesting firmware version...\n");

			break;
		case ABORT_LONG_PRESS:
			send_data[1] = 0x01;
			send_data[2] = 0xf3;
			printf("Aborting long press...\n");

			break;
		case SEND_COMMAND:
			if (housecode == NULL) {
				fprintf(stderr, "housecode needed!\n");
				exit(EXIT_FAILURE);
			}
			if (addr == -1) {
				fprintf(stderr, "address needed!\n");
				exit(EXIT_FAILURE);
			}

			if (repeat) {
				send_data[1] = 0x07;
				send_data[2] = 0xf2;
				send_data[8] = repeat;
				printf("Sending 0x%02x 0x%02x (%.2fs) to address %d (hc: 0x%02x%02x) (repeated %d times)\n",
					command, extension, EXT_TO_SEC(extension), addr,
					housecode[0], housecode[1],
					repeat);
			} else {
				send_data[1] = 0x06;
				send_data[2] = 0xf1;
				printf("Sending 0x%02x 0x%02x (%.2fs) to address %d (hc: 0x%02x%02x)\n",
					command, extension, EXT_TO_SEC(extension), addr,
					housecode[0], housecode[1]);
			}
			send_data[3] = housecode[0];
			send_data[4] = housecode[1];
			send_data[5] = addr;
			send_data[6] = command;
			send_data[7] = extension;

			break;
		case NO_ACTION:
		default:
			fprintf(stderr, "No action specified!\n\n");
			syntax(argv[0]);
			exit(EXIT_FAILURE);
			break;
	}

	err = libusb_init(NULL);
	if (err != 0) {
		fprintf(stderr, "Can't initialize libusb: %s\n", usb_strerror(err));
		return EXIT_FAILURE;
	}

	devh = fs20pcs_find();
	if (!devh) {
		fprintf(stderr, "Can't find/open fs20pcs!\n");
		return EXIT_FAILURE;
	}

	if (fs20pcs_send(devh, send_data) == 0) {
		fprintf(stderr, "Can't communicate with fs20pcs!\n");
		return EXIT_FAILURE;
	}

	libusb_close(NULL);

	return EXIT_SUCCESS;
}
