CFLAGS=-O2 -Wall -I/opt/local/include -g
LDFLAGS=-L/opt/local/lib
LDLIBS=-lusb-1.0 -lm
CC=gcc

all: fs20pcs

fs20pcs: fs20pcs.o

clean:
	rm -f fs20pcs.o fs20pcs
